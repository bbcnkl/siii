import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../shared/user/user.service';
import { User } from '../shared/user/user.model';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { BaseComponent } from '../base/base.component';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent extends BaseComponent implements OnInit {

    private userForm = {
        email: '',
        password: ''
    };
    failed = false;

    constructor(private userService: UserService,
                private router: Router) {
        super();
    }

    ngOnInit() {
    }
    onClear() {
        // this.loginForm.reset();
    }

    login() {
        const formData = new FormData();
        formData.append('email',  this.userForm.email);
        formData.append('password',  this.userForm.password);
        this.userService.login(formData)
            .subscribe((data: any) => {
                if (data.status !== 1) {
                    this.failed = true;
                } else {
                    this.failed = false;
                    localStorage.setItem('user', JSON.stringify(data.data));
                    this.user = data.data;
                    this.router.navigate(['/']);
                }
            });
    }

}
