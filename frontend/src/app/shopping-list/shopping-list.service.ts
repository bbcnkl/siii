import {Item} from '../admin/items/items.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import {Injectable} from "@angular/core";
import { forEach } from '@angular/router/src/utils/collection';

@Injectable()
export class ShoppingListService {

    private itemsforsale: Item [] = [];
    private url = 'http://78.46.117.245/siii/backend/index.php/Products/';

    constructor(private http: HttpClient) {}

    getSale() {
        return this.itemsforsale.slice();
    }

    addItem(item: Item) {
        let index = -1;
        //uklonjena greska umesto foreach iskoristila sam for i podesila da je i tipa 'number'
        for(let i=0; i < this.itemsforsale.length; i++) {
            if ( this.itemsforsale[i]['id'] === item['id']) {
                index = i;
            }
        }
        if (index == -1) {
            this.itemsforsale.push(item);
        } else {
            this.itemsforsale[index]['amount']++;
        }
    }

    deleteItem(item: Item) {
        this.itemsforsale.splice(this.itemsforsale.indexOf(item), 1);

    }

    sellProduct(items: any): Observable<any> {
        const formData = new FormData();
        formData.append('products',  JSON.stringify(items));
        return this.http.post(this.url + 'SellProduct', formData);
    }
}
