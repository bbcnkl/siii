import {Component, OnInit, OnDestroy} from '@angular/core';
import {ShoppingListService} from './shopping-list.service';
import {Item} from '../admin/items/items.model';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-shopping-list',
    templateUrl: './shopping-list.component.html',
    styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit, OnDestroy {

    itemsForSale: Item[];
    private item: Item;

    constructor(private slService: ShoppingListService) {
    }

    ngOnInit() {
        this.item = new Item(null, '', '', '', '', null, 0, 0, 0, null, 0);

        this.itemsForSale = this.slService.getSale();
    }

    deleteProduct(item: Item) {
        this.itemsForSale.splice(this.itemsForSale.indexOf(item), 1);
        this.slService.deleteItem(item);
    }

    imageModal(item: Item) {
        this.item = item;
        $('#imageModal').modal('show');
    }

    sellProducts() {
        this.slService.sellProduct(this.itemsForSale)
            .subscribe((data: any) => {
            if (data.status) {
                this.itemsForSale = [];
                let htmlTemp = 'Račun u pdf formatu možete preuzeti na : <a target="_blank" href="http://78.46.117.245/siii/frontend/uploads/' + data.file  +'">linku</a> ';

                Swal.fire({
                    title: 'Uspešno prodato',
                    type: 'success',
                    html: htmlTemp,
                    showCloseButton: true,
                    showCancelButton: false,
                    focusConfirm: false,
                    confirmButtonText:
                        '<i class="fa fa-thumbs-up"></i> Zatvori!',
                    cancelButtonText:
                        '<i class="fa fa-thumbs-down"></i>'
                });
            } else {
                alert('Došlo je do greške');
            }
        });
    }

    ngOnDestroy() {
        // this.subscription.unsubscribe();
    }
}
