import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';


import {AppComponent} from './app.component';
import {AdminItemsComponent} from './admin/items/items.component';
import {OptionsComponent} from './admin/options/options.component';
import {GroupsComponent} from './admin/groups/groups.component';
import {ManufacturersComponent} from './admin/manufacturers/manufacturers.component';
import {ShoppingListComponent} from './shopping-list/shopping-list.component';
import {DropdownDirective} from './share/dropdown.directive';
import {ShoppingListService} from './shopping-list/shopping-list.service';
import {AppRoutingModule} from './app-routing.module';
import {SidebarComponent} from './shared/sidebar/sidebar.component';
import {HeaderComponent} from './shared/header/header.component';
import {FooterComponent} from './shared/footer/footer.component';
import {LoginComponent} from './login/login.component';
import {UserService} from './shared/user/user.service';
import { HelpComponent } from './help/help.component';
import { OrdersComponent } from './admin/orders/orders.component';
import { OrdersService } from './admin/orders/orders.service';
import { SoldItemsService } from './admin/sold-items/sold-items.service';
import { ItemService } from './admin/items/item.service';

import * as bootstrap from 'bootstrap';
import * as $ from 'jquery';
import Swal from 'sweetalert2';
import { SoldItemsComponent } from './admin/sold-items/sold-items.component';


@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        AdminItemsComponent,
        OptionsComponent,
        GroupsComponent,
        ShoppingListComponent,
        DropdownDirective,
        SidebarComponent,
        FooterComponent,
        LoginComponent,
        ManufacturersComponent,
        HelpComponent,
        OrdersComponent,
        SoldItemsComponent,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        AppRoutingModule,
        ReactiveFormsModule,
        FormsModule
    ],
    providers: [ShoppingListService, UserService, OrdersService, SoldItemsService,
        ItemService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
