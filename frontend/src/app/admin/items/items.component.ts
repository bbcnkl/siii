import {Component, OnInit} from '@angular/core';
import {ItemService} from './item.service';
import {Router} from '@angular/router';
import {Item} from './items.model';
import {BaseComponent} from '../../base/base.component';
import Swal from 'sweetalert2';
import { SoldItem } from './soldItems.model';

@Component({
    selector: 'app-items',
    templateUrl: './items.component.html',
    styleUrls: ['./items.component.css'],
    providers: [
        ItemService
    ]
})
export class AdminItemsComponent extends BaseComponent implements OnInit {

    private items: any[] = [];
    private soldItems : any[] = [];
    private kolicine: number[] = [];
    private item: Item;
    private tmpItem: any;
    private tmpItem2: Item;
    private groups: any[] = [];
    private options: any[] = [];
    private manufacturers: any[] = [];
    private edit = false;
    /////// NOVO ///////
    private order = false;
    public selectedGroup = 0;
    public searchTerm = '';

    constructor(
        private itemService: ItemService,
        private router: Router
    ) {
        super();
    }

    ngOnInit() {
        if (!this.checkIfLogged()) {
            this.router.navigate(['/login']);
        }
        this.item = new Item(null, '', '', '', '', null, 0, 0, 0, null, 0);
        this.tmpItem2 = new Item(null, '', '', '', '', null, 0, 0, 0, null, 0);

        // Get products from backend
        this.listAllProducts();

    }

    listAllProducts() {
        this.itemService.getItems()
            .subscribe((data: any) => {
                if (data.status) {
                    this.items = data.data; // ovde je potrebno uraditi deserializaciju ako zelimo da items bude tipa Item
                }
            });
        this.itemService.getManufacturers()
            .subscribe((data: any) => {
                if (data.status) {
                    this.manufacturers = data.data;
                }
            });
        this.itemService.getGroups()
            .subscribe((data: any) => {
                if (data.status) {
                    this.groups = data.data;
                }
            });
        this.itemService.getSoldItems()
            .subscribe((data: any) => {
                if (data.status) {
                    this.soldItems = data.data;
                    this.sumIt(this.soldItems);
                }               
            });
    }

    deleteProduct(id) {
        this.itemService.deleteProduct(id)
            .subscribe((data: any) => {
                if (data.status) {
                    for (let i = 0; i < this.items.length; i++) {
                        if (this.items['id'] == id) {
                            this.items.splice(i, 1);
                        }
                    }
                    /////// NOVO dodata funcija listAllProducts()///////
                    this.listAllProducts();
                } else {
                    alert('Došlo je do greške');
                }
            });
    }

    sumIt(soldItems)
    {        
        for (let i in this.items)
        {
            this.item.kolicina = 0;
            for (let j in this.soldItems) {
                if (this.items[i]['id'] == this.soldItems[j]['product_id']) {
                   this.item.kolicina = this.item.kolicina + this.soldItems[j]['quantity']; 
                }
            } 
        }        
    }

    addNewProduct() {
        for (let i in this.item) {
            if ((i != 'id' && i != 'last_sold_date') && this.item[i] == null || this.item[i] === '') {
                alert('Sva polja su obavezna. Popunite polje: ' + i);
                return 0;
            }
        }
        this.itemService.addProduct(this.item)
            .subscribe((data: any) => {
                if (data.status) {
                    this.listAllProducts();
                    $('.modal').modal('hide');
                } else {
                    alert('Došlo je do greške');
                }
            });
    }

    discount(price, discount, group_id, manufacture_id) {
        let realPrice = Number(price) - Number(price) * Number(discount) / 100;
        let groupDiscount = 0;
        let manufactureDiscount = 0;
        for (let i in this.groups) {
            if (this.groups[i]['id'] == group_id) {
                groupDiscount = Number(this.groups[i]['discount']);
            }
        }

        for (let i in this.manufacturers) {
            if (this.manufacturers[i]['id'] == manufacture_id) {
                manufactureDiscount = Number(this.manufacturers[i]['discount']);
            }
        }

        realPrice = Number(realPrice) - Number(realPrice) * Number(groupDiscount) / 100;
        realPrice = Number(realPrice) - Number(realPrice) * Number(manufactureDiscount) / 100;
        return realPrice;
    }

    editProductBackend() {
        for (let i in this.item) {
            if ((i != 'last_sold_date') && this.item[i] == null || this.item[i] === '') {
                alert('Sva polja su obavezna. Popunite polje: ' + i);
                return 0;
            }
        }
        this.itemService.editProduct(this.item)
            .subscribe((data: any) => {
                if (data.status) {
                    this.listAllProducts();
                    $('.modal').modal('hide');
                } else {
                    alert('Došlo je do greške');
                }
            });
    }

    orderProduct(item) {
        this.edit = false;
        this.order = true;

        this.tmpItem = this.clone(item);
        this.tmpItem['price'] = this.discount(item['price'], item['discount'], item['group_id'], item['manufacture_id']);
        this.tmpItem['amount'] = this.tmpItem2['amount'];


        if (this.tmpItem.amount == 0) {
            Swal.fire({
                title: 'Popunite količinu za proizvod ' + this.tmpItem.name + '',
                type: 'warning',

            });
        }
        else if (this.tmpItem.amount > 0) {
            this.itemService.addItemToOrderList(this.tmpItem);
            Swal.fire({
                title: 'Proizvod ' + this.tmpItem.name + ' je dodat u listu naručenih proizvoda sa količinom ' + this.tmpItem.amount + '',
                type: 'success',

            });
            $('#productModalOrder').modal('hide');
        }
        else {
            Swal.fire({
                title: 'Količina za proizvod mora biti veća od nule',
                type: 'warning',

            });
        }

    }

    editProduct(item) {
        this.item = item;
        this.edit = true;
        this.order = false;
        $('#productModal').modal('show');
    }

    imageModal(item) {
        this.item = item;
        $('#imageModal').modal('show');
    }

    addNewModal() {
        this.edit = false;
        /////// NOVO ///////
        this.order = false;
        this.item = new Item(null, '', '', '', '', null, 0, 0, 0, null, 0);
        $('#productModal').modal('show');
    }

    checkIfSoldIn30Days(date) {
      if (date == null) {
        return false;
      }
      let dateSold = new Date(date);
      var d = new Date();
      d.setMonth(d.getMonth() - 1);
      d.setHours(0, 0, 0);
      d.setMilliseconds(0);

      if (dateSold > d) {
        return true;
      }
      return false;
    }

    getDiscount(date) {

        let dateSold = new Date(date);

        var dThreeMonth = new Date();
        dThreeMonth.setMonth(dThreeMonth.getMonth() - 3);
        dThreeMonth.setHours(0, 0, 0);
        dThreeMonth.setMilliseconds(0);

        if (dateSold < dThreeMonth) {
            return 15;
        }

        var dTwoMonth = new Date();
        dTwoMonth.setMonth(dTwoMonth.getMonth() - 2);
        dTwoMonth.setHours(0, 0, 0);
        dTwoMonth.setMilliseconds(0);

        if (dateSold < dTwoMonth) {
            return 10;
        }

        var dOneMonth = new Date();
        dOneMonth.setMonth(dOneMonth.getMonth() - 1);
        dOneMonth.setHours(0, 0, 0);
        dOneMonth.setMilliseconds(0);

        if (dateSold < dOneMonth) {
            return 5;
        }
    }

    setDiscount(item) {
        item['discount'] = (Number(item['discount']) + this.getDiscount(item['last_sold_date']));
        this.itemService.editProduct(item)
            .subscribe((data: any) => {
                if (data.status) {
                    this.listAllProducts();
                    $('.modal').modal('hide');
                } else {
                    alert('Došlo je do greške');
                }
            });
    }

    /////// NOVO //////
    sellProductModal(item) {
        this.edit = false;
        this.order = false;
        this.tmpItem = this.clone(item);
        this.tmpItem['price'] = this.discount(item['price'], item['discount'], item['group_id'], item['manufacture_id']);
        this.tmpItem['amount'] = 1;
        this.itemService.addItemToShoppingList(this.tmpItem);
        Swal.fire({
            title: 'Proizvod je dodat u korpu.',
            // text: 'Do you want to continue',
            type: 'success',
            // confirmButtonText: 'Cool'
        });
    }

    /////// NOVO ///////
    orderProductModal(item) {
        this.edit = false;
        this.order = true;
        //preuzmi sve vrednosti selektovanog itema
        this.item = item;

        /////// NOVI ID ///////
        $('#productModalOrder').modal('show');
    }

    getManufacturer(id) {
        for (let i in this.manufacturers) {
            if (this.manufacturers[i]['id'] == id) {
                return this.manufacturers[i]['name'];
            }
        }
    }

}
