import { NumberValueAccessor } from "@angular/forms/src/directives";

export class Item {
    public id: string;
    public name: string;
    public price: number;
    public amount: number;
    public description: string;
    public image: string;
    public link: string;
    public warranty: string;
    public discount: number;
    public manufacture_id: number;
    public group_id: number;
    public kolicina : number;

    constructor (id: string, name: string, description: string,
                 image: string, link: string, warranty: string,
                 amount: number, price: number, discount: number,
                 manufacture_id: number, group_id: number) {

        this.id = id
        this.name = name
        this.description = description
        this.image = image;
        this.link = link;
        this.warranty = warranty;
        this.amount = amount;
        this.price = price;
        this.discount = discount;
        this.manufacture_id = manufacture_id;
        this.group_id = group_id;

    }

}
