export class SoldItem {
    public id: string;
    public quantity: string;
    public bill_id: string;

    constructor (id: string, quantity: string, bill_id: string) {

        this.id = id
        this.quantity = quantity
        this.bill_id = bill_id

    }

}
