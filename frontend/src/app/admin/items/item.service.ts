import { Item } from './items.model';
import { Injectable} from '@angular/core';
import { Subject } from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

///// NOVO ////
import { ShoppingListService } from '../../shopping-list/shopping-list.service';
import { OrdersService } from '../orders/orders.service';
 

@Injectable()
export class ItemService{


    private url = 'http://78.46.117.245/siii/backend/index.php/Products/';
    private items: Item[] = []
    
    ///// NOVO slService , private slService: ShoppingListService////
    constructor(private http: HttpClient, private slService: ShoppingListService, private ordersService: OrdersService) {}

    getItems(): Observable<any> {
      return this.http.get(this.url + 'GetProducts');
    }

    getSoldItems(): Observable<any> {
        return this.http.get(this.url + 'GetSProducts');
      }

    deleteProduct(id: any): Observable<any> {
        const formData = new FormData();
        formData.append('id',  id);
        return this.http.post(this.url + 'DeleteProduct', formData);
    }

    addProduct(item: Item): Observable<any> {
        const formData = new FormData();
        formData.append('name',  item.name);
        formData.append('description',  item.description);
        formData.append('price',  String(item.price));
        formData.append('amount',  String(item.amount));
        formData.append('discount',  String(item.discount));
        formData.append('image',  item.image);
        formData.append('link',  item.link);
        formData.append('warranty',  item.warranty);
        formData.append('manufacture_id',  String(item.manufacture_id));
        formData.append('group_id',  String(item.group_id));
        return this.http.post(this.url + 'AddProduct', formData);
    }
     /////// NOVO ///////
    /*orderProduct(item: Item): Observable<any> {
       
    }*/

    editProduct(item: Item): Observable<any> {
        const formData = new FormData();
        formData.append('id',  item.id);
        formData.append('name',  item.name);
        formData.append('description',  item.description);
        formData.append('price',  String(item.price));
        formData.append('amount',  String(item.amount));
        formData.append('discount',  String(item.discount));
        formData.append('image',  item.image);
        formData.append('link',  item.link);
        formData.append('warranty',  item.warranty);
        formData.append('manufacture_id',  String(item.manufacture_id));
        formData.append('group_id',  String(item.group_id));
        return this.http.post(this.url + 'EditProducts', formData);
    }

    addState(state: any): Observable<any> {
        const formData = new FormData();
        formData.append('amount',  state.amount);
        formData.append('price',  state.price);
        formData.append('product_id',  state.product_id);
        formData.append('discount',  state.discount);
        return this.http.post(this.url + 'AddItem', formData);
    }

    getState(id: any): Observable<any> {
        return this.http.get(this.url + 'ListItems/' + id);
    }

    getManufacturers(): Observable<any> {
        return this.http.get('http://78.46.117.245/siii/backend/index.php/Manufacturers/GetManufactures');
    }

    getGroups(): Observable<any> {
        return this.http.get('http://78.46.117.245/siii/backend/index.php/Groups/GetGroups');
    }

    ////// NOVO //////
    addItemToShoppingList(item: Item) {
        this.slService.addItem(item);
    }
    addItemToOrderList(item: Item) {
        this.ordersService.addOrder(item).subscribe();
    }
}
