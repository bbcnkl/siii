import { Component, OnInit } from '@angular/core';
import { SoldItemsService } from './sold-items.service';

@Component({
  selector: 'app-sold-items',
  templateUrl: './sold-items.component.html',
  styleUrls: ['./sold-items.component.css']
})
export class SoldItemsComponent implements OnInit {

  bills: any[];
  soldItems: any[];

  constructor(private soldItemsService: SoldItemsService) { }

  ngOnInit() {
    this.loadAll();
  }

  loadAll() {
      this.soldItemsService.getBills()
          .subscribe((data: any) => {
              if (data.status) {
                  this.bills = data.data;

              this.soldItemsService.getSoldItems()
                  .subscribe((data: any) => {
                      if (data.status) {
                          this.soldItems = data.data; // ovde je potrebno uraditi deserializaciju ako zelimo da items bude tipa Item
                      }
                  });
                }
          });

  }

}
