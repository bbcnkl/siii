import {Item} from '../items/items.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import {Injectable} from "@angular/core";
import { forEach } from '@angular/router/src/utils/collection';

@Injectable()
export class SoldItemsService {

    private itemsfororder: Item [] = [];
    private url = 'http://78.46.117.245/siii/backend/index.php/Products/';

    constructor(private http: HttpClient) {}

    getSoldItems() {
      return this.http.get(this.url + 'GetSoldProducts');
    }

    getBills(): Observable<any> {
      return this.http.get(this.url + 'GetBills');
    }

    /*sellProduct(items: any): Observable<any> {
        const formData = new FormData();
        formData.append('products',  JSON.stringify(items));
        return this.http.post(this.url + 'SellProduct', formData);
    }*/
}
