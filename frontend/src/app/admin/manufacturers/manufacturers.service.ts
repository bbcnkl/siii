import { Manufacturer } from './manufacturer.model';
import { Injectable} from '@angular/core';
import { Subject } from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';


@Injectable()
export class ManufacturersService {


    private url = 'http://78.46.117.245/siii/backend/index.php/Manufacturers/';

    constructor(private http: HttpClient) {}

    getManufacturers(): Observable<any> {
      return this.http.get(this.url + 'GetManufactures');
    }

    addManufacturer(manufacturer: Manufacturer): Observable<any> {
        const formData = new FormData();
        formData.append('name',  manufacturer['name']);
        formData.append('email',  manufacturer['email']);
        formData.append('address',  manufacturer['address']);
        formData.append('discount', manufacturer['discount']);
        return this.http.post(this.url + 'AddManufacturer', formData);
    }

    editManufacturer(manufacturer: Manufacturer): Observable<any> {
        const formData = new FormData();
        formData.append('id',  manufacturer['id']);
        formData.append('name',  manufacturer['name']);
        formData.append('email',  manufacturer['email']);
        formData.append('address',  manufacturer['address']);
        formData.append('discount', manufacturer['discount']);
        return this.http.post(this.url + 'UpdateManufacturer', formData);
    }

    deleteManufacturer(id: any): Observable<any> {
        const formData = new FormData();
        formData.append('id',  id);
        return this.http.post(this.url + 'DeleteManufacturer', formData);
    }

}
