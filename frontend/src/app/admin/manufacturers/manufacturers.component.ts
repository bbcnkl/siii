import { Component, OnInit } from '@angular/core';
import { ManufacturersService } from './manufacturers.service';
import { Router } from '@angular/router';
import { Manufacturer } from './manufacturer.model';

@Component({
    selector: 'app-manufacturers',
    templateUrl: './manufacturers.component.html',
    styleUrls: ['./manufacturers.component.css'],
    providers: [
        ManufacturersService
    ]
})
export class ManufacturersComponent implements OnInit {
    private manufacturers: any[] = [];
    private manufacturer: Manufacturer = new Manufacturer(null, '', '', '', null);
    private newManufacturer: Manufacturer = new Manufacturer(null, '', '', '', 0);

    constructor(private manufacturersService: ManufacturersService) {
    }

    ngOnInit() {
        this.listAllManufacturers();
    }

    listAllManufacturers() {
        this.manufacturersService.getManufacturers()
            .subscribe((data: any) => {
                if (data.status) {
                    this.manufacturers = data.data;
                }
            });
    }

    deleteManufacturer(id) {
        this.manufacturersService.deleteManufacturer(id)
            .subscribe((data: any) => {
                if (data.status) {
                    this.listAllManufacturers();
                } else {
                    alert(data.message);
                }
            });
    }

    editManufacturerSide(manufacturer) {
        this.manufacturer = manufacturer;
    }

    editManufacturer() {
        this.manufacturersService.editManufacturer(this.manufacturer)
            .subscribe((data: any) => {
                if (data.status) {
                    this.listAllManufacturers();
                    this.manufacturer = new Manufacturer(null, '', '', '', null);
                } else {
                    alert(data.message);
                }
            });
    }

    insertManufacturer() {
        if (this.newManufacturer.name.trim() !== '') {
            this.manufacturersService.addManufacturer(this.newManufacturer)
                .subscribe((data: any) => {
                    this.listAllManufacturers();
                    this.newManufacturer = new Manufacturer(null, '', '', '', 0);
                });
        }
    }

}
