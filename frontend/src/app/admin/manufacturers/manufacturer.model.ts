export class Manufacturer {
    public id: string;
    public name: string;
    public address: string;
    public email: string;
    public discount: any;

    constructor (id: string, name: string, address: string, email: string, discount: any) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.email = email;
        this.discount = discount;

    }

}
