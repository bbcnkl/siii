
import {Component, OnInit, OnDestroy} from '@angular/core';
import {ShoppingListService} from '../../shopping-list/shopping-list.service';
import {Item} from '../items/items.model'
import {Subscription} from 'rxjs';
import Swal from 'sweetalert2';
import { OrdersService } from './orders.service';
import { ItemService } from '../items/item.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
    private item: Item;
    items: any[];
    orders: any[];
    manufacturers: any[];

    constructor(private orderService: OrdersService, private itemService: ItemService) {
    }

    ngOnInit() {
        this.item = new Item(null, '', '', '', '', null, 0, 0, 0, null, 0);

        // this.itemsForOrder = this.orderService.getOrder();
        this.listAllOrderds();
    }

    listAllOrderds() {
        this.orderService.getOrders()
            .subscribe((data: any) => {
                if (data.status) {
                    this.orders = data.data;
                }
            });
        this.itemService.getItems()
            .subscribe((data: any) => {
                if (data.status) {
                    this.items = data.data; // ovde je potrebno uraditi deserializaciju ako zelimo da items bude tipa Item
                }
            });
        this.itemService.getManufacturers()
            .subscribe((data: any) => {
                if (data.status) {
                    this.manufacturers = data.data;
                }
            });
    }

    getProductName(id) {
        for (let i in this.items) {
            if (this.items[i]['id'] == id) {
                return this.items[i]['name'];
            }
        }
    }

    getManufacturer(id) {
        for (let i in this.manufacturers) {
            if (this.manufacturers[i]['id'] == id) {
                return this.manufacturers[i]['name'];
            }
        }
    }
    getManufacturerMail(id) {
        for (let i in this.manufacturers) {
            if (this.manufacturers[i]['id'] == id) {
                return this.manufacturers[i]['email'];
            }
        }
    }

    deleteOrder(id) {
        this.orderService.deleteOrder(id)
            .subscribe((data: any) => {
                this.listAllOrderds();
                if (data.status) {
                } else {
                    alert('Došlo je do greške');
                }
            });
    }

    sendOrder(item) {
        let email = this.getManufacturerMail(item.manufacturer_id);

        let message = "Ime proizvoda: " + this.getProductName(item.product_id);
        message += "<br>količina: " + item.amount + "";
        message += "<br><br> PCStore";
        this.orderService.sendOrder(email, message).subscribe((data: any) => {
            if (data.status) {
                Swal.fire({
                    title: 'Uspešno ste naručili proizvod.',
                    type: 'success',
                });
            } else {
                alert('Došlo je do greške');
            }
        });
    }


}
