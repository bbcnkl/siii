import {Item} from '../items/items.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import {Injectable} from "@angular/core";
import { forEach } from '@angular/router/src/utils/collection';

@Injectable()
export class OrdersService {

    private url = 'http://78.46.117.245/siii/backend/index.php/Orders/';

    constructor(private http: HttpClient) {}

    getOrders(): Observable<any> {
        return this.http.get(this.url + 'GetOrder');
    }

    addOrder(item: Item): Observable<any> {
        const formData = new FormData();
        formData.append('product_id',  item.id);
        formData.append('amount',  String(item.amount));
        formData.append('manufacturer_id',  String(item.manufacture_id));
        formData.append('user_id',  String(JSON.parse(localStorage.getItem("user")).user_id));
        console.log(formData)
        return this.http.post(this.url + 'AddOrder', formData);
    }

    deleteOrder(id: any): Observable<any> {
        const formData = new FormData();
        formData.append('id',  id);
        return this.http.post(this.url + 'DeleteOrder', formData);
    }

    sendOrder(email: string, message: string): Observable<any> {
        const formData = new FormData();
        formData.append('email',  email);
        formData.append('message',  message);
        return this.http.post(this.url + 'SendOrder', formData);
    }
}
