import {Component, OnInit} from '@angular/core';
import {OptionsService} from './options.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-options',
    templateUrl: './options.component.html',
    styleUrls: ['./options.component.css'],
    providers: [
        OptionsService
    ]
})
export class OptionsComponent implements OnInit {
    private options: any[] = [];
    public name = '';

    constructor(private optionsService: OptionsService,
                private router: Router
    ) {
    }

    ngOnInit() {
        this.listAllOptions();
    }

    listAllOptions() {
        this.optionsService.getItems()
            .subscribe((data: any) => {
                if (data.status) {
                    this.options = data.data;
                }
            });
    }

    deleteOption(id) {
        this.optionsService.deleteOption(id)
            .subscribe((data: any) => {
                if (data.status) {
                    this.listAllOptions();
                } else {
                    alert('Doslo je do greske');
                }
            });
    }

    insertOption() {
        if (this.name.trim() !== '') {
            this.optionsService.insertOption(this.name)
                .subscribe((data: any) => {
                    this.listAllOptions();
                    this.name = '';
                });
        }
    }

}
