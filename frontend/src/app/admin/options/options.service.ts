import { Option } from './options.model';
import { Injectable} from '@angular/core';
import { Subject } from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';


@Injectable()
export class OptionsService {


    private url = 'http://78.46.117.245/siii/backend/index.php/Options/';

    constructor(private http: HttpClient) {}

    getItems(): Observable<any> {
      return this.http.get(this.url + 'GetOptions');
    }

    insertOption(name: string): Observable<any> {
        const formData = new FormData();
        formData.append('name',  name);
        return this.http.post(this.url + 'AddOption', formData);
    }

    deleteOption(id: any): Observable<any> {
        const formData = new FormData();
        formData.append('id',  id);
        return this.http.post(this.url + 'DeleteOption', formData);
    }

}
