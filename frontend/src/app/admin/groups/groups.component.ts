import {Component, OnInit} from '@angular/core';
import {GroupsService} from './groups.service';
import {Router} from '@angular/router';
import { Group } from './groups.model';

@Component({
    selector: 'app-options',
    templateUrl: './groups.component.html',
    styleUrls: ['./groups.component.css'],
    providers: [
        GroupsService
    ]
})
export class GroupsComponent implements OnInit {
    private groups: any[] = [];
    private editGroup: Group = new Group(null, null, null);
    public name = '';
    public discount = 0;

    constructor(private groupsService: GroupsService,
                private router: Router
    ) {
    }

    ngOnInit() {
        this.listAllGroups();
    }

    listAllGroups() {
        this.groupsService.getGroups()
            .subscribe((data: any) => {
                if (data.status) {
                    this.groups = data.data;
                }
            });
    }

    deleteGroup(id) {
        this.groupsService.deleteGroup(id)
            .subscribe((data: any) => {
                if (data.status) {
                    this.listAllGroups();
                } else {
                    alert('Doslo je do greske');
                }
            });
    }

    editGroups() {
        this.groupsService.editGroup(this.editGroup)
            .subscribe((data: any) => {
                if (data.status) {
                    this.listAllGroups();
                    this.editGroup = new Group(null, null, null);
                } else {
                    alert('Doslo je do greske');
                }
            });
    }

    insertGroup() {
        if (this.name.trim() !== '') {
            this.groupsService.insertGroup(this.name, String(this.discount))
                .subscribe((data: any) => {
                    this.listAllGroups();
                    this.name = '';
                    this.discount = 0;
                });
        }
    }

}
