import { Group } from './groups.model';
import { Injectable} from '@angular/core';
import { Subject } from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';


@Injectable()
export class GroupsService {


    private url = 'http://78.46.117.245/siii/backend/index.php/Groups/';

    constructor(private http: HttpClient) {}

    getGroups(): Observable<any> {
      return this.http.get(this.url + 'GetGroups');
    }

    insertGroup(name: string, discount: string): Observable<any> {
        const formData = new FormData();
        formData.append('name',  name);
        formData.append('discount', discount);
        return this.http.post(this.url + 'AddGroup', formData);
    }

    editGroup(group: Group): Observable<any> {
        const formData = new FormData();
        formData.append('id',  group['id']);
        formData.append('name',  group['name']);
        formData.append('discount', group['discount']);
        return this.http.post(this.url + 'UpdateGroup', formData);
    }

    deleteGroup(id: any): Observable<any> {
        const formData = new FormData();
        formData.append('id',  id);
        return this.http.post(this.url + 'DeleteGroup', formData);
    }

}
