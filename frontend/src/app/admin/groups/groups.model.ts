export class Group {
    public id: string
    public name: string
    public discount: any

    constructor (id: string, name: string, discount: any) {
        this.id = id;
        this.name = name;
        this.discount = discount;

    }

}
