import {Component, Injector, Input, Output, OnInit, EventEmitter, ElementRef, ViewChild} from '@angular/core';
import {Observable} from 'rxjs';

@Component({
    selector: 'app-base',
    template: ''
})
export class BaseComponent implements OnInit {

    protected readGranted = false;
    public ROLES = {
        '1': 'Admin',
        '2': 'Vlasnik',
        '3': 'Komercijalista',
        '4': 'Radnik',
    };
    public user = {};

    constructor() {
    }

    ngOnInit() {
    }

    checkIfLogged() {
        if (localStorage.getItem('user')) {
            this.user = JSON.parse(localStorage.getItem('user'))

            // alert('Dobro došli ' + this.user['first_name'] + '. Vi ste: ' + this.ROLES[this.user['role']])
            return true;
        } else {
            return false;
        }
    }


    clone(obj) {
        if (null == obj || "object" != typeof obj) return obj;
        var copy = obj.constructor();
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
        }
        return copy;
    }
}