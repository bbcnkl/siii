import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../base/base.component';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.css']
})
export class HelpComponent extends BaseComponent implements OnInit {

  private admin = false;
  private komercijalista = false;
  private radnik = false;

  public name = '';
  public role = '';

  constructor() { 
    super(); 
  }

  ngOnInit() {
    
    if (!this.checkIfLogged()) {
    } else {
        this.name = this.user['first_name'] + ' ' + this.user['last_name'];
        this.role = this.ROLES[this.user['role']];
    }

  }

  
}
