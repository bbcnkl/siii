import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminItemsComponent } from './admin/items/items.component';
import { OptionsComponent } from './admin/options/options.component';
import { GroupsComponent } from './admin/groups/groups.component';
import { OrdersComponent } from './admin/orders/orders.component';
import { SoldItemsComponent } from './admin/sold-items/sold-items.component';
import { ManufacturersComponent } from './admin/manufacturers/manufacturers.component';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { HelpComponent } from './help/help.component';

import { LoginComponent } from './login/login.component';


const appRoutes: Routes = [
    { path: '', redirectTo: '/products', pathMatch: 'full' },
    { path: 'products', component: AdminItemsComponent},
    { path: 'shopping-list', component: ShoppingListComponent },
    { path: 'login', component: LoginComponent },
    { path: 'options', component: OptionsComponent},
    { path: 'groups', component: GroupsComponent},
    { path: 'orders', component: OrdersComponent},
    { path: 'sold-products', component: SoldItemsComponent},
    { path: 'manufacturers', component: ManufacturersComponent},
    { path: 'help', component: HelpComponent }

];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})

export class AppRoutingModule {

}
