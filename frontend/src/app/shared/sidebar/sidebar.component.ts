import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../base/base.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent extends BaseComponent implements OnInit {

  private logged = false;
  constructor(
      private router: Router
  ) {
    super();
  }

  ngOnInit() {
      if (!this.checkIfLogged()) {
          this.router.navigate(['/login']);
      }
      this.logged = true;
  }

  logout() {
      this.user = {}
      localStorage.removeItem('user');
      this.router.navigate(['/login']);
  }

}
