import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  dateMessage : string;

  constructor() 
  {
   setInterval( () => {
     let currentDate = new Date();
     this.dateMessage = currentDate.toDateString() + ' ' + currentDate.toLocaleTimeString();
   }, 1000 ) 

  }

  ngOnInit() {
  }

}
