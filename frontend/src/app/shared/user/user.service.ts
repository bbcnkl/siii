import {User} from './user.model';
import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';


@Injectable()
export class UserService {
    private user: User;
    private url = 'http://78.46.117.245/siii/backend/index.php/User/Login';

    constructor(private http: HttpClient) {}

    getUsers() {
        return this.user;
    }

    addUser(user: User) {
        this.user = user;
    }

    login(user: FormData): Observable<any> {
        const httpOptions = {
            headers: new HttpHeaders({
                // 'Content-Type':  'application/x-www-form-urlencoded'
                // 'Content-Type':  'application/form-data'
            })
        };
        return this.http.post(this.url, user, httpOptions);
    }
}
