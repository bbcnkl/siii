export class User {
    public static id: number;
    public static firstName: string;
    public static lastName: string;
    public static token: string;
    public email: string;
    public password: string;
    public static role = 0;

    constructor(email: string, password: string) {
        this.email = email;
        this.password = password;
    }
    

    set setId(id: number) {
        User.id = id;
    }
}
