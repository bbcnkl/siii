import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../base/base.component';

@Component({
    selector: 'app-header',
    styles: [`
    `],
    templateUrl: './header.component.html'
})
export class HeaderComponent extends BaseComponent  implements OnInit {

    public name = '';
    public role = '';
    constructor(
    ) {
        super();
    }
    ngOnInit() {
        if (!this.checkIfLogged()) {
        } else {
            this.name = this.user['first_name'] + ' ' + this.user['last_name'];
            this.role = this.ROLES[this.user['role']];
        }
    }
}