<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Orders_mod extends CI_Model
{

	private $_dbTableOrders = 'orders';
	private $_dbTableInvoices = 'orders';

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }


    public function AddOrder($order)
    {
        $this->db->insert($this->_dbTableOrders, $order);
        $msg = "Proizvod: " . $order['product_id'] . "\nKoličina: " . $order['amount'];
        mail("bbcnkl@gmail.com","Nova porudzbina",$msg);
    }


    public function GetOrder()
    {
        $this->db->select('orders.id, orders.product_id, orders.amount, orders.manufacturer_id, orders.user_id, orders.datetime, users.first_name, users.last_name')
            ->from('orders')
            ->join('users', 'orders.user_id = users.id');
        $result = $this->db->get();

        return $result->result_array();
    }


    public function DeleteOrder($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->_dbTableOrders);
    }

		public function GetInvoices()
		{

		}





}
