<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Groups_mod extends CI_Model
{

    private $_dbTableGroups = 'groups';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }


    public function AddGroup($data)
    {
        $this->db->insert($this->_dbTableGroups, $data);
    }


    public function GetGroups()
    {
        $query = $this->db->get($this->_dbTableGroups);
        return $query->result_array();
    }


    public function DeleteGroup($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->_dbTableGroups);
    }

    public function UpdateGroup($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update($this->_dbTableGroups, $data);
    }


}