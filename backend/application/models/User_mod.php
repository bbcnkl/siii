<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_mod extends CI_Model
{

    private $_dbTableUsers = 'users';
    private $_dbTableUsersToken = "users_token";

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }


    public function GetIp()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }


    public function Login($email, $password)
    {
        $pass_new = sha1($password);

        $query = $this->db->select("*")
            ->from($this->_dbTableUsers)
            ->where("(email = '" . $this->db->escape_str($email) . "')")
            ->where('password', $pass_new)
            ->get();

        if ($query->num_rows() == 1) {
            $user = $query->row();
            $response["id"] = $user->id;
            $response["first_name"] = $user->first_name;
            $response["last_name"] = $user->last_name;
            $response["email"] = $user->email;
            $response["role"] = $user->role;
        } else {
            $response = false;
        }
        return $response;
    }

    public function Logout($token)
    {
        $this->db->where('token', $token);
        $this->db->delete($this->_dbTableUsersToken);
    }

    public function CheckToken($user_id, $token)
    {
        $query = $this->db->select("*")
            ->from($this->_dbTableUsersToken)
            ->where('user_id', $user_id)
            ->where('token', $token)
            ->get();

        if ($query->num_rows() == 1) {
            $result = true;
        } else {
            $result = false;
        }

        return $result;
    }

    public function LoginWithToken($user_id, $token)
    {
        if ($this->CheckToken($user_id, $token)) {

            $query = $this->db->select("*")
                ->from($this->_dbTableUsers)
                ->where('id', $user_id)
                ->get();

            if ($query->num_rows() == 1) {
                $user = $query->row();
                $response["id"] = $user->id;
                $response["role"] = $user->role;
                $response["first_name"] = $user->first_name;
                $response["last_name"] = $user->last_name;
                $response["email"] = $user->email;
            } else {
                $response = false;
            }

            return $response;
        }

    }

    public function SetUserToken($user_id, $token)
    {
        $date = date("Y-m-d H:i:s");
        $data = array(
            'user_id' => $user_id,
            'token' => $token,
            'date' => $date,
            'ip' => $this->GetIp()

        );

        $this->db->insert($this->_dbTableUsersToken, $data);
    }

    public function checkIfEmailExist($data)
    {
        $query = $this->db->select("*")
            ->from($this->_dbTableUsers)
            ->where('email', $data['email'])
            ->where('id !=', $data['id'])
            ->get();

        if ($query->num_rows() == 1) {
            $result = true;
        } else {
            $result = false;
        }

        return $result;
    }

    public function getUserId($token)
    {
        $query = $this->db->select("user_id")
            ->from($this->_dbTableUsersToken)
            ->where('token', $token)
            ->get();


        if ($query->num_rows() == 1) {
            $user = $query->row();
            $result = $user->user_id;
        } else {
            $result = 0;
        }

        return $result;
    }


    public function AddUserModel($user)
    {
        $this->db->insert($this->_dbTableUsers, $user);
    }

} // end class User_mod

?>
