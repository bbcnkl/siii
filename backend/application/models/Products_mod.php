<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Products_mod extends CI_Model
{

    private $_dbTableUsers = 'users';
    private $_dbTableUsersToken = "users_token";
    private $_dbTableProducts = 'products';
    private $_dbTableOptions = 'options';
    private $_dbTableItems = 'items';
    private $_dbTableBills= 'bills';
    private $_dbTableSoldProducts = 'sold_products';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function GetProducts()
    {
        $query = $this->db->get($this->_dbTableProducts);
        return $query->result_array();
    }

    public function GetProductById($id) {
        $this->db->select('id, name, amount');
        $query = $this->db->get_where($this->_dbTableProducts, array('id' => $id));
        return $query->row();
    }


    public function AddProduct($product)
    {
        $this->db->insert($this->_dbTableProducts, $product);
    }

    public function EditProducts($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update($this->_dbTableProducts, $data);
    }

    // @TODO for sold products

    public function DeleteProduct($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->_dbTableProducts);
    }

    public function AddItem($item)
    {
        $this->db->insert($this->_dbTableItems, $item);
    }

    public function AddBill($bill)
    {
        $this->db->insert($this->_dbTableBills, $bill);
        $insert_id = $this->db->insert_id();
        return  $insert_id;
    }

    public function CountSoldProducts($id) {
        $this->db->select('product_id, quantity');
        $query = $this->db->get_where($this->_dbTableSoldProducts, array('product_id' => $id));
        return $query->result_array();
    }

    public function AddSoldProduct($product)
    {
        $this->db->insert($this->_dbTableSoldProducts, $product);
    }

    public function GetBills()
    {
        $query = $this->db->get($this->_dbTableBills);
        return $query->result_array();
    }

    public function GetSoldProducts()
    {
        $query = $this->db->get($this->_dbTableSoldProducts);
        return $query->result_array();
    }

    public function ListItems($product_id)
    {

        //$this->db->where('product_id', $product_id);
        //$query = $this->db->get($this->_dbTableItems);

        $query = $this->db->get_where($this->_dbTableItems, array('product_id' => $product_id));

        return $query->result_array();
    }
}
?>
