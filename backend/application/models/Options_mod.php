<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Options_mod extends CI_Model
{

	private $_dbTableOptions = 'options';

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

	public function AddOption($name)
    {
        $this->db->insert($this->_dbTableOptions, $name);
    }


    public function GetOptions()
    {
        $query = $this->db->get($this->_dbTableOptions);

        return $query->result_array();
    }


    public function DeleteOption($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->_dbTableOptions);
    }




}