<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Manufacturers_mod extends CI_Model
{

    private $_dbTableManufacturers = 'manufacturers';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }


    public function AddManufacturer($data)
    {
        $this->db->insert($this->_dbTableManufacturers, $data);

    }


    public function ListManufacturers()
    {
        $query = $this->db->get($this->_dbTableManufacturers);

        return $query->result_array();
    }


    public function DeleteManufacturer($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->_dbTableManufacturers);
    }

    public function UpdateManufacturer($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update($this->_dbTableManufacturers, $data);

    }


}