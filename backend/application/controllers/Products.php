<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends MY_Controller {

	public function __construct()
	{
			parent::__construct();

	}

	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function AddProduct()
    {
        $product = array(
            'name' =>  $this->input->post('name'),
            'description' => $this->input->post('description'),
            'price' => $this->input->post('price'),
            'discount' => $this->input->post('discount'),
            'amount' => $this->input->post('amount'),
            'image' => $this->input->post('image'),
            'link' => $this->input->post('link'),
            'warranty' => $this->input->post('warranty'),
            'manufacture_id' => $this->input->post('manufacture_id'),
            'group_id' => $this->input->post('group_id')
        );

        if ($product['name'] != '' && $product['image'] != '' && $product['manufacture_id'] != null) {

            $this->products_mod->AddProduct($product);
            $response = array(
                'status' => 1,
                'message' => "Uspešno ste dodali proizvod."
            );

        } else {
            $response = array(
                'status' => 0,
                'message' => "Niste uneli neki parametar",
                'data' => $product
            );
        }

        echo json_encode($response);
    }

    public function GetProducts()
    {
    	$products = $this->products_mod->GetProducts();

		$response = array(
	        'status' => 1,
	        'data' => $products
	    );

    	echo json_encode($response);
    }
    public function GetSoldProducts()
    {
    	$products = $this->products_mod->GetSoldProducts();

		$response = array(
	        'status' => 1,
	        'data' => $products
	    );

    	echo json_encode($response);
    }

    public function EditProducts()
    {
        $product = array(
            'id' =>  $this->input->post('id'),
            'name' =>  $this->input->post('name'),
            'description' => $this->input->post('description'),
            'price' => $this->input->post('price'),
            'discount' => $this->input->post('discount'),
            'amount' => $this->input->post('amount'),
            'image' => $this->input->post('image'),
            'link' => $this->input->post('link'),
            'warranty' => $this->input->post('warranty'),
            'manufacture_id' => $this->input->post('manufacture_id'),
            'group_id' => $this->input->post('group_id')
        );
        $this->products_mod->EditProducts($product);

        $response = array(
            'status' => 1,
            'message' => 'Uspešno ste izmenili proizvod.'
        );

        echo json_encode($response);

    }

    public function DeleteProduct()
    {
		$id = $this->input->post('id');

		if($id != null)
		{

		$this->products_mod->DeleteProduct($id);

    	$response = array(
                'status' => 1,
                'message' => "Uspešno ste obrisali prozivod"
            );

		} else
		{
			$response = array(
                'status' => 0,
                'message' => "Došlo je do greške pri brisanju proizvoda.",
                'id' => $id
            );
		}

    	echo json_encode($response);
    }

    public function CountSoldProducts($id) 
    {
        $products = $this->products_mod->CountSoldProducts($id);
        $numb = 0;
        for($cnt = 0; $cnt < count($products); $cnt++) {
            $numb = (int)$numb + (int)$products[$cnt]["quantity"];
           
        }

        $response = array(
            'status' => 1,
            'message' => "Uspesno izbrojano",
            'quantity' => $numb,
            'prod' => count($products)
        );

        echo json_encode($response);

    }

    // @TODO for sold products

    public function SellProduct()
    {
        $this->load->helper('date');
        $this->load->library('fpdf_gen');
        $this->fpdf->SetFont('Arial', 'B', 14);	//text style
        $now = date('m-d-Y-His');
        
        $this->fpdf->cell(10, 10, 'PC Store');
        $this->fpdf->Ln();
        $this->fpdf->cell(10, 10, 'Racun broj: ' . $now);

        $this->fpdf->Ln();


        $products = json_decode($this->input->post('products'));
        $price = 0;
        $this->fpdf->SetFont('Arial', 'B', 12);	//text style
        $pdfCnt = 50;
        $pdfCnt = $pdfCnt + 10;
        
        for($cnt = 0; $cnt < count($products); $cnt++) {
            $tmp = (int)$products[$cnt]->amount * (float)$products[$cnt]->price;
            $price = (float)$price + (float)$tmp;
            $this->fpdf->Ln();
            $pdfCnt = $pdfCnt + 10;
            $this->fpdf->cell($pdfCnt, 10, 'Proizvod: ' . $products[$cnt]->name);
            $this->fpdf->Ln();
            $pdfCnt = $pdfCnt + 10;			//new line
            $this->fpdf->cell($pdfCnt, 10, 'Kolicina: ' . $products[$cnt]->amount);
            $this->fpdf->Ln();
            $pdfCnt = $pdfCnt + 10;
            $this->fpdf->cell($pdfCnt, 10, 'Cena po jedinici: ' . $products[$cnt]->price);
            $pdfCnt = $pdfCnt + 20;

            // update product
            $product = $this->products_mod->GetProductById($products[$cnt]->id);
            $new_amount =  (int)$product->amount - (int)$products[$cnt]->amount;
            $edit_product = array(
                'id' => $products[$cnt]->id,
                'amount' => $new_amount,
								'last_sold_date' => date("Y-m-d h:i:sa")
            );
            $this->products_mod->EditProducts($edit_product);

        }
        $this->fpdf->SetFont('Arial', 'B', 14);
        $this->fpdf->Ln();
        $this->fpdf->cell($pdfCnt, 10, 'Ukupno: ' . $price);




        $filename = "/var/www/html/siii/frontend/uploads/".$now.".pdf";
        $this->fpdf->Output($filename,'F');

        $bill = array(
            'price' => $price,
            'file' => $now.".pdf"
        );

        $bill_id = $this->products_mod->AddBill($bill);
        for($i = 0; $i < count($products); $i++) {
            $sold = array(
                'bill_id' => $bill_id,
                'quantity' => (int)$products[$i]->amount,
                'product_id' => $products[$i]->id,
            );
            $this->products_mod->AddSoldProduct($sold);
        }


        $response = array(
            'status' => 1,
            'message' => "Uspešno",
            'data' => $products,
            'price' => $price,
            'count' => count($products),
            'file' => $now.".pdf"
        );

        echo json_encode($response);
    }

    public function AddSoldProduct()
    {
        $item = array(
            'amount' =>  $this->input->post('amount'),
            'price' => $this->input->post('price'),
            'product_id' => $this->input->post('product_id'),
            'discount' => $this->input->post('discount')
        );

        if ($item['amount'] != '' && $item['price'] != '') {

            $this->products_mod->AddItem($item);
            $response = array(
                'status' => 1,
                'message' => "Uspešno"
            );

        } else {
            $response = array(
                'status' => 0,
                'message' => "Neuspesno"
            );
        }

        echo json_encode($response);
    }

    public function ListItems($product_id)
    {

        $products = $this->products_mod->ListItems($product_id);

        $response = array(
            'status' => 1,
            'data' => $products
        );

        echo json_encode($response);
    }

    public function EditItems($id)
    {
        $item = $this->products_mod->EditItems($id);

        $response = array(
            'status' => 1,
            'data' => $item
        );

        echo json_encode($response);

    }

		public function GetBills()
    {
        $products = $this->products_mod->GetBills();
        $response = array(
            'status' => 1,
            'data' => $products
        );
        echo json_encode($response);
    }

		public function GetSProducts()
    {
        $products = $this->products_mod->GetSoldProducts();
        $response = array(
            'status' => 1,
            'data' => $products
        );
        echo json_encode($response);
    }




}
