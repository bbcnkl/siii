<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	public function __construct()
	{
			parent::__construct();

	}

	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function Login()
	{
			$email = $this->input->post('email');
			$pass = $this->input->post('password');

			if ($email != '' && $pass != '') {
					$user = $this->user_mod->Login($email, $pass);

					if ($user != false) {
							$token = $this->encrypt->encode($user["id"], $this->config->item('encryption_key'));

//							@ TODO tokenizacija
//							$this->user_mod->SetUserToken($user["id"], $token);

							$response = array(
									'status' => 1,
									'data' => array(
											'token' => $token,
											'user_id' => $user["id"],
											'first_name' => $user["first_name"],
											'last_name' => $user["last_name"],
											'role' => $user["role"],
											'email' => $email,

									),
									'message' => 'Uspešno ste se ulogovali'
							);

					} else {
							$response = array(
									'status' => 0,
									'message' => "E-mail ili lozinka nisu ispravni."
							);

					}
			} else {
					$response = array(
							'status' => 0,
							'message' => "Мolimo unesite e-mail i lozinku",
                            'email' => $email,
                            'password' => $pass
					);
			}

			echo json_encode($response);
	}

	public function AddUser()
    {
        $user = array(
            'email' =>  $this->input->post('email'),
            'password' => $this->input->post('password'),
            'first_name' => $this->input->post('first_name'),
            'last_name' => $this->input->post('last_name'),
            'role' => $this->input->post('role')
        );

        if ($user['email'] != '' && $user['password'] != '') {

            $this->user_mod->AddUserModel($user);
            $response = array(
                'status' => 1,
                'message' => "Uspešno ste dodali korisnika."
            );

        } else {
            $response = array(
                'status' => 0,
                'message' => "Niste uneli e-mail i lozinku"
            );
        }

        echo json_encode($response);
    }
}
