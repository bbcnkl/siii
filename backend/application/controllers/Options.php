<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
defined('BASEPATH') OR exit('No direct script access allowed');

class Options extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

    }

    public function AddOption()
    {
        $option = array('name' => $this->input->post('name'));

        $this->options_mod->AddOption($option);

        $response = array(
            'status' => 1,
            'message' => "Uspešno ste dodali opciju."
        );

        echo json_encode($response);
    }


    public function GetOptions()
    {
        $options = $this->options_mod->GetOptions();

        $response = array(
            'status' => 1,
            'data' => $options
        );

        echo json_encode($response);
    }


    public function DeleteOption()
    {
        $id = $this->input->post('id');

        if ($id != null) {

            $this->options_mod->DeleteOption($id);

            $response = array(
                'status' => 1,
                'message' => "Uspešno ste obrisali opciju."
            );

        } else {
            $response = array(
                'status' => 0,
                'message' => "Došlo je do greške pri brisanju opcije",
                'id' => $id
            );
        }

        echo json_encode($response);
    }


}