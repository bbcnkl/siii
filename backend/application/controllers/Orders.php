<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends MY_Controller {

	public function __construct()
	{
			parent::__construct();

	}

	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function AddOrder()
	{
		$order = array(
            'product_id' =>  $this->input->post('product_id'),
            'amount' => $this->input->post('amount'),
            'manufacturer_id' => $this->input->post('manufacturer_id'),
            'user_id' => $this->input->post('user_id'),
            'datetime' => date("Y-m-d h:i:sa")
            
        );

		if ($order['product_id'] != '' && $order['amount'] != '' && $order['manufacturer_id'] != '' && $order['user_id'] != '' && $order['datetime'] != '') {

            $this->orders_mod->AddOrder($order);
            $response = array(
                'status' => 1,
                'message' => "Uspešno"
            );

        } else {
            $response = array(
                'status' => 0,
                'message' => "Niste uneli neki parametar",
                'data' => $order
            );
        }

        echo json_encode($response);

	}

	public function GetOrder()
    {
    	$order = $this->orders_mod->GetOrder();

		$response = array(
	        'status' => 1,
	        'data' => $order
	    );

    	echo json_encode($response);
    }

    public function DeleteOrder()
    {
		$id = $this->input->post('id');

		if($id != null)
		{

		$this->orders_mod->DeleteOrder($id);

    	$response = array(
                'status' => 1,
                'message' => "Uspešno ste obrisali porudzbinu"
            );

		} else
		{
			$response = array(
                'status' => 0,
                'message' => "Došlo je do greške pri brisanju porudzbine.",
                'id' => $id
            );
		}

    	echo json_encode($response);
    }

    public function SendOrder()
    {
        $email = $this->input->post('email');
        $message = $this->input->post('message');


        if ($this->_send_email($email, "pcstoreoffice@gmail.com", "Naručivanje proizvoda", $message)) {

            $response = array(
                'status' => 1,
                'message' => "Uspešno ste narucili porudzbinu"
            );
        } else {
            $response = array(
                'status' => 0,
                'message' => "Došlo je do greške pri narucivanju porudzbine.",
            );

        }



        echo json_encode($response);
    }

    private function _send_email($emails, $from, $subject, $message)
    {

        $this->load->library('email');

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.gmail.com',
            'smtp_port' => 465,
                'smtp_user' => 'pcstoreoffice@gmail.com',
            'smtp_pass' => 'ABC123...',
            'mailtype' => 'html',
            'charset' => 'utf-8'
        );
        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");

        $this->email->to($emails);
        $this->email->from($from, 'PCStore');
        $this->email->subject($subject);
        $this->email->message($message);

        if (!$this->email->send()) {
            // Generate error (by CI)
            //throw new Exception("Email cannot be sent. Try again or contact admin!");
            //echo $this->email->print_debugger();
            return false;
        }

        return true;
    }






}