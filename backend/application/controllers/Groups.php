<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
defined('BASEPATH') OR exit('No direct script access allowed');

class Groups extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

    }

    public function AddGroup()
    {
        $data = array(
            'name' => $this->input->post('name'),
            'discount' => $this->input->post('discount')
        );


        $this->groups_mod->AddGroup($data);

        $response = array(
            'status' => 1,
            'message' => "Uspešno je dodata grupa"
        );

        echo json_encode($response);
    }


    public function GetGroups()
    {
        $operation = $this->groups_mod->GetGroups();

        $response = array(
            'status' => 1,
            'data' => $operation
        );

        echo json_encode($response);
    }


    public function DeleteGroup()
    {
        $id = $this->input->post('id');

        if ($id != null) {

            $this->groups_mod->DeleteGroup($id);

            $response = array(
                'status' => 1,
                'message' => "Uspešno ste obrisali grupu"
            );

        } else {
            $response = array(
                'status' => 0,
                'message' => "Došlo je do greške pri brisanju grupe",
                'id' => $id
            );
        }

        echo json_encode($response);
    }

    public function UpdateGroup()
    {
        $data = array(
            'id' => $this->input->post('id'),
            'name' => $this->input->post('name'),
            'discount' => $this->input->post('discount')
        );
        if ($data['id'] != null && $data['name'] != null && $data['discount'] != null) {

            $this->groups_mod->UpdateGroup($data);

            $response = array(
                'status' => 1,
                'message' => "Uspešno je izmenjena grupa"
            );

        } else {
            $response = array(
                'status' => 0,
                'message' => "Došlo je do greške pri menjanju grupe",
                'id' => $data['id']
            );
        }

        echo json_encode($response);
    }


}