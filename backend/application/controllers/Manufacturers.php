<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
defined('BASEPATH') OR exit('No direct script access allowed');

class Manufacturers extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

    }

    public function AddManufacturer()
    {
        $data = array(
            'name' => $this->input->post('name'),
            'logo' => $this->input->post('logo'),
            'address' => $this->input->post('address'),
            'email' => $this->input->post('email'),
            'discount' => $this->input->post('discount')
        );

        $this->manufacturers_mod->AddManufacturer($data);

        $response = array(
            'status' => 1,
            'message' => "Uspešno je dodat partner."
        );

        echo json_encode($response);
    }


    public function GetManufactures()
    {
        $array = $this->manufacturers_mod->ListManufacturers();

        $response = array(
            'status' => 1,
            'data' => $array
        );

        echo json_encode($response);
    }


    public function DeleteManufacturer()
    {
        $id = $this->input->post('id');

        if ($id != null) {

            $this->manufacturers_mod->DeleteManufacturer($id);

            $response = array(
                'status' => 1,
                'message' => "Uspešno ste obrisali partnera"
            );

        } else {
            $response = array(
                'status' => 0,
                'message' => "Došlo je do greške pri brisanju partnera.",
                'id' => $id
            );
        }

        echo json_encode($response);
    }

    public function UpdateManufacturer()
    {
        $data = array(
            'id' =>  $this->input->post('id'),
            'name' => $this->input->post('name'),
            'logo' => $this->input->post('logo'),
            'address' => $this->input->post('address'),
            'email' => $this->input->post('email'),
            'discount' => $this->input->post('discount')
        );


        if (!is_null($data['id']) && !is_null($data['name']) && !is_null($data['address']) && !is_null($data['email'])) {

            $this->manufacturers_mod->UpdateManufacturer($data);

            $response = array(
                'status' => 1,
                'message' => "Uspešno je izmenjen partner."
            );

        } else {
            $response = array(
                'status' => 0,
                'message' => "Došlo je do greške pri izmeni partnera."
            );
        }

        echo json_encode($response);
    }


}