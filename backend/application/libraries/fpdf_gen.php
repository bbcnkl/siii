<?php if (!defined('BASEPATH')) exit ('No Access');


class fpdf_gen
{
	public function __construct()
	{
		require_once '/var/www/html/siii/backend/application/third_party/fpdf/fpdf.php';
		$pdf = new FPDF();

		$pdf->AddPage();

		$CI = get_instance();
		$CI->fpdf = $pdf;
	}
}