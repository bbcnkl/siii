<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
class MY_Controller extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

        $this->load->model('user_mod');
        $this->load->library('encrypt');
        $this->load->model('products_mod');
        $this->load->model('options_mod');
        $this->load->model('groups_mod');
        $this->load->model('manufacturers_mod');
        $this->load->model('orders_mod');
	}

    public function get_ip()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

}

/* End of file MY_Controller.php */
/* Location: ./core/MY_Controller.php */
